# -*- coding: utf-8 -*-
from odoo import http

# class Apple(http.Controller):
#     @http.route('/apple/apple/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/apple/apple/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('apple.listing', {
#             'root': '/apple/apple',
#             'objects': http.request.env['apple.apple'].search([]),
#         })

#     @http.route('/apple/apple/objects/<model("apple.apple"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('apple.object', {
#             'object': obj
#         })